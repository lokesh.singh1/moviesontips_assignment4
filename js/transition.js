 var MOVIEScoming = "http://localhost:3000/movies-coming";
 var MOVIESintheaters = "http://localhost:3000/movies-in-theaters";
 var MOVIESindiatop = "http://localhost:3000/top-rated-india";
 var MOVIES = "http://localhost:3000/top-rated-movies";
 var MOVIESfavourite = "http://localhost:3000/favourit";
 var map = new Map();

function SearchMovies(url){
  fetch(url)
  .then((res)=> res.json())
  .then((data)=>{
   var component = document.getElementById("movieselect");
   component.setAttribute('class','imagesection')
   component.innerHTML="";

       data.forEach(element => {
          var box = document.createElement("div");
          box.setAttribute('class','box');
          var image=document.createElement("img");
          image.setAttribute("src",element.posterurl);
          image.setAttribute('class', 'imageContainer');
          image.setAttribute('id', 'movieBlock');

          var button = document.createElement("BUTTON")
          button.setAttribute('id','buttonid') 
          var text = document.createTextNode("Add to Fav");
          button.appendChild(text);
          button.onclick = function(event){
              event.preventDefault();
              addtoFav(element);
              alert("Movie is added to favourite list");
              map.set( element.id ,element)
          }
          box.append(image);
          box.append(button);
          box.onclick = function(){
              details(element);
          }
         component.append(box);
       });
  }).catch();   
}

window.onload=()=>{
  SearchMovies(MOVIEScoming);
}

function addtoFav(element){
  fetch("http://localhost:3000/favourit", {
    method: 'POST',  
    headers: {
      'Content-Type': 'application/json'
    },  
    redirect: "manual",  
    body: JSON.stringify(element)
  });
}

function deletefromFAV(element){
  fetch("http://localhost:3000/favourit/"+element.id, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(element)
  });
}

function details(element){
  var content=document.getElementById("moviedetails");
  content.innerHTML="";
  var cont1=document.createElement("P");
  var cont2=document.createElement("P");
  var cont3=document.createElement("P");
  var cont4=document.createElement("P");
  cont1.setAttribute("class", "contentClass");
  cont2.setAttribute("class", "contentClass");
  cont3.setAttribute("class", "contentClass");
  cont4.setAttribute("class", "contentClass");
  cont1.innerText="Title : " + element.title;
  cont2.innerText="year : "+element.year;
  cont3.innerText="Imdb-rating : "+element.imdbRating;
  cont4.innerText="Storyline : "+element.storyline;
  content.append(cont1);
  content.append(cont2);
  content.append(cont3);
  content.append(cont4);
}

function FavMovies(url){
  fetch(url)
  .then((res)=> res.json())
  .then((data)=>{
   var component = document.getElementById("movieselect");
   component.setAttribute('class','imagesection')
   component.innerHTML="";

       data.forEach(element => {
          var box = document.createElement("div");
          box.setAttribute('class','box');

          var image=document.createElement("img");
          image.setAttribute("src",element.posterurl);
          image.setAttribute('class', 'imageContainer');
          image.setAttribute('id', 'movieBlock');

          var button = document.createElement("BUTTON")
          button.setAttribute('class', 'paraClass');
          button.setAttribute('id','buttonid2') 
          var text = document.createTextNode("Remove From Fav");
          button.appendChild(text);
          button.onclick = function(event){
            event.preventDefault();
             deletefromFAV(element);
             alert("Movie is deleted from favourite list");
          }
          box.append(image);
          box.append(button);
          component.append(box);
       });
  }).catch();   
}