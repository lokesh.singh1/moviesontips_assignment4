# MoviesOnTips_Assignment4

=> Run "data.json" JSON server on port:3000 (Api calls for all categories is taken as per this port number)

=> User will be shown with the title of our web application "Movies On Tips" and will be given 5 choices as form of buttons. 

=> On default, user will be shown the "MoviesUpcoming" section.

=> All the movies of a particular section will be shown in a grid on the same page itself as a grid, user can scroll down the page.

=> Clicking a particular movie poster will reveal it's all information available at the top of movies section.

=> Every movie will be having a "Add to Fav" button below it's poster, pressing which user will get an alert that it's been added to favrouite list.

=> Unlike every choice given, Favourite section will be having a button "Remove from Fav", pressing which movie will be removed from the favourite list.

#Note 
=> Favourite list is been handled by using a map that will prevent the duplication of entry into the Favrouite list.
=> Poster image is fetched through the url given in the data.json to display the user.


